package lilacapps.example.com.activityasdialogsample;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by 1021422 on 10/29/2017.
 */

public class DialogActivity extends AppCompatActivity implements OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);

        ListView listView = (ListView) findViewById(R.id.listOfPrefs);
        ArrayList<String> userPrefs = getIntent().getStringArrayListExtra("userPrefs");
        Collections.sort(userPrefs);
        String test = getIntent().getExtras().getString("test");
        Log.i("test",test);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,userPrefs);
        Log.i(getLocalClassName(),Integer.toString(arrayAdapter.getCount()));
        listView.setAdapter(arrayAdapter);

        Button ok_btn = (Button) findViewById(R.id.okButton);
        Button cancel_btn = (Button) findViewById(R.id.cancelButton);

        ok_btn.setOnClickListener(this);
        cancel_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.okButton:
                showToastMsg("ok button clicked and dialog closes");
                this.finish();
                break;
            case R.id.cancelButton:
                showToastMsg("cancel button clicked; the dialog is not closed");
//                this.finish();
                break;
        }
    }

    private void showToastMsg(String message){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }
}
