package lilacapps.example.com.activityasdialogsample;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;

public class MainActivity extends AppCompatActivity {
    private String TAG = "Preferences List";
    private Gson gson;
    private SharedPreferences sharedPrefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        savePreferences();

        Button button = (Button) findViewById(R.id.showPrefs);

        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                ArrayList<String> userPrefs = getSharedPreferences();
                for(String temp: userPrefs ){
                    Log.i("List ",temp);
                }
                Intent intent = new Intent(MainActivity.this,DialogActivity.class);
                intent.putStringArrayListExtra("userPrefs",userPrefs);
                intent.putExtra("test","test");
                startActivity(intent);
            }
        });

    }
    private void savePreferences(){
        Set<String> set = new HashSet<String>();

        ArrayList<String> arrayList =
                new ArrayList<>();
        for(int i=0; i<20; i++){
            arrayList.add("This is preference "+i);
        }
        set.addAll(arrayList);
        sharedPrefs = getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putStringSet(TAG,set);
        editor.commit();
    }
    private ArrayList<String> getSharedPreferences(){
        sharedPrefs = getDefaultSharedPreferences(MainActivity.this);
        Set<String> set = sharedPrefs.getStringSet(TAG,null);

        for(String temp: set ){
            Log.i("Set ",temp);
        }
        ArrayList<String> finalArrayList = new ArrayList<>(set);

        return finalArrayList;
    }
}
